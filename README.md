# ntfy-for-cloudron ![](https://ci.eyen.ca/api/badges/cloudron/ntfy.sh/status.svg)

This is a first attempt to package https://ntfy.sh for Cloudron.
It's working, subject to the following :
* server is (intentionally) closed: a default user of admin/changeme is created. To change it, you will need to open a terminal and follow the instructions [here](https://ntfy.eyen.ca/docs/config/#users-and-roles)
* contrary to most examples in ntfy.sh docs, notifications should include https
  `curl -d "Hi" https://server.name/topicname`
* if you change the domain address, you will need to manually update the `/app/data/config/server.yml` file or delete it to let it be recreated.

Help to sort those issues out will be gratefully appreciated.

## build script cld.sh
`cld.sh` is an optional utility script to simplify build and deployment.
Just type at Terminal `./cld.sh <yourreponame>/<appname>:<tag>` 
Script will :
- perform docker build
- perform docker push
- initiate cloudron install

You will be asked for `Location:` : this is where the app will be available, e.g. app.domain.tld.

Requirements :
- have Docker installed and be logged in
- have a Docker repo
- have cloudron CLI installed and be logged in

