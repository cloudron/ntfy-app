The app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme<br/>

Please change the admin password immediately following these [instructions](https://docs.cloudron.io/apps/ntfy/#change-password).

**Note:** By design, the web interface is accessible by all and can be used to interact with
one or more ntfy servers.

