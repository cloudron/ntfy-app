#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    safe = require('safetydance'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

const username = 'admin',
    password = 'changeme';

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };

    let browser, app;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function checkWebApp() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//span[contains(text(), "All notifications")]'));
    }

    async function subscribeToTopic() {
        await browser.get('https://' + app.fqdn);
        await waitForElement(By.xpath('//span[contains(text(), "Subscribe to topic")]'));
        await browser.findElement(By.xpath('//span[contains(text(), "Subscribe to topic")]')).click();
        await waitForElement(By.xpath('//input[@id="topic"]'));
        await browser.findElement(By.xpath('//input[@id="topic"]')).sendKeys('cloudron-topic');
        await browser.sleep(1000);
        await browser.findElement(By.xpath('//button[contains(text(), "Subscribe")]')).click();
        await browser.sleep(1000);
        await waitForElement(By.xpath('//h2[contains(text(), "Login required")]'));
        await browser.findElement(By.xpath('//input[@id="username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@id="password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[contains(text(), "Login")]')).click();
        await browser.sleep(1000);
        await waitForElement(By.xpath('//span[contains(text(), "cloudron-topic")]'));
    }

    async function sendMessage(message) {
        execSync(`cloudron exec --app ${app.location} -- ntfy publish cloudron-topic ${message}`, EXEC_ARGS);
    }

    async function cannotSendMessage(message) {
        const output = safe.child_process.execSync(`curl -s -u ${username}:xxxx${password} -d "${message}" https://${app.fqdn}/cloudron-topic`, { encoding: 'utf8' });
        expect(output).to.contain('unauthorized');
    }

    async function checkMessage(message) {
        await browser.get(`https://${app.fqdn}/cloudron-topic`);
        await waitForElement(By.xpath(`//p[contains(text(), "${message}")]`));
    }

    // xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can see the web app', checkWebApp);
    it('can subscribe to topic', subscribeToTopic);
    it('send message', sendMessage.bind(null, 'message 1'));
    it('got message', checkMessage.bind(null, 'message 1'));
    it('cannot message', cannotSendMessage.bind(null, 'message fail'));

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });

    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
        execSync('cloudron install --location ' + LOCATION, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('got message', checkMessage.bind(null, 'message 1'));

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can subscribe to topic', subscribeToTopic);
    it('got message', checkMessage.bind(null, 'message 1'));
    it('send message', sendMessage.bind(null, 'message 2'));
    it('got message', checkMessage.bind(null, 'message 2'));
    it('cannot message', cannotSendMessage.bind(null, 'message fail'));

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });

    // test update
    it('can install app for update', async function () {
        browser.manage().deleteAllCookies();
        browser.executeScript('window.open("chrome://settings/clearBrowserData?search=history");');
        console.log('Manually clear the browser history, will wait for 60s');
        execSync('cloudron install --appstore-id sh.ntfy.cloudronapp --location ' + LOCATION, EXEC_ARGS);
        await browser.sleep(60000);
    });
    it('can get app information', getAppInfo);
    it('can see the web app', checkWebApp);
    it('can subscribe to topic', subscribeToTopic); // if this fails, it's because browser history was not cleared. otherwise, we have to implement unsubscribe
    it('send message', sendMessage.bind(null, 'message 3'));

    it('can update', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron update --app ' + app.id, EXEC_ARGS);
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];
        expect(app).to.be.an('object');
    });
    it('got message', checkMessage.bind(null, 'message 3'));
    it('cannot message', cannotSendMessage.bind(null, 'message fail'));

    it('uninstall app', function () { execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS); });
});
