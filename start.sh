#!/bin/bash

set -eu

setup_admin() {
    while [[ ! -f "/app/data/users.db" ]]; do
        echo "==> Waiting for users database to appear"
        sleep 1
    done

    echo "==> Add initial admin user"
    NTFY_PASSWORD=changeme ntfy user add --role=admin admin
}

mkdir -p /app/data/config /app/data/attachments

if [[ ! -f /app/data/config/server.yml ]]; then
    cp /app/pkg/server.yml.template /app/data/config/server.yml
    setup_admin & # the sqlite db (users.db) is created on first run by the app
fi

[[ ! -f /app/data/config/client.yml ]] && cp /app/pkg/client.yml.template /app/data/config/client.yml

yq eval -i ".base-url=\"${CLOUDRON_APP_ORIGIN}\"" /app/data/config/server.yml
yq eval -i ".default-host=\"${CLOUDRON_APP_ORIGIN}\"" /app/data/config/client.yml
    
yq eval -i ".smtp-sender-addr=\"${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTPS_PORT}\"" /app/data/config/server.yml
yq eval -i ".smtp-sender-user=\"${CLOUDRON_MAIL_SMTP_USERNAME}\"" /app/data/config/server.yml
yq eval -i ".smtp-sender-pass=\"${CLOUDRON_MAIL_SMTP_PASSWORD}\"" /app/data/config/server.yml
yq eval -i ".smtp-sender-from=\"${CLOUDRON_MAIL_FROM}\"" /app/data/config/server.yml

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Starting NFTY Server"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/ntfy serve

