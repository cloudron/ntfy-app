FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/pkg /app/code

WORKDIR /app/code

# renovate: datasource=github-releases depName=binwiederhier/ntfy versioning=semver extractVersion=^v(?<version>.+)$
ARG NTFY_VERSION=2.11.0

RUN curl -L https://github.com/binwiederhier/ntfy/releases/download/v${NTFY_VERSION}/ntfy_${NTFY_VERSION}_linux_amd64.tar.gz | tar zxf - --strip-components 1 -C /app/code

RUN ln -s /app/data/config /etc/ntfy

COPY start.sh server.yml.template client.yml.template /app/pkg/

# add code to PATH to allow ntfy to be called directly
ENV PATH=/app/code:$PATH 

CMD [ "/app/pkg/start.sh" ]
