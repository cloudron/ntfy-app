ntfy (pronounce: notify) is a simple HTTP-based pub-sub notification service. It allows you to send
notifications to your phone or desktop via scripts from any computer, entirely without signup or cost.

